package com.example.ud6_ejemplo3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class FragmentDos : Fragment()  {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // "Inflamos" el Fragment
        val view = inflater.inflate(R.layout.fragment_dos, container, false)

        return view
    }
}