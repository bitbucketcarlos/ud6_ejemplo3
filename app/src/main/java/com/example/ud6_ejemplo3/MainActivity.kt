package com.example.ud6_ejemplo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.ud6_ejemplo3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        val fragmentUno = FragmentUno()
        val fragmentDos = FragmentDos()
        var fragmentMostrado: Int

        // Preparamos la transacción entre Fragments
        val fragmentManager = supportFragmentManager
        var fragmentTransaction = fragmentManager.beginTransaction()

        // Añadimos el Fragment uno
        fragmentTransaction.add(R.id.frameLayout, fragmentUno)

        // Llamamos al método addToBackStack antes del commit para poder volver a atrás
        fragmentTransaction.addToBackStack(null)

        fragmentTransaction.commit()

        fragmentMostrado = 1

        binding.boton.setOnClickListener {
            // Preparamos la transacción entre Fragments
            fragmentTransaction = fragmentManager.beginTransaction()

            if(fragmentMostrado == 1){
                // Reemplazamos por el Fragment dos
                fragmentTransaction.replace(R.id.frameLayout, fragmentDos)
                fragmentMostrado = 2;
            }
            else{
                // Reemplazamos por el Fragment uno
                fragmentTransaction.replace(R.id.frameLayout, fragmentUno)
                fragmentMostrado = 1
            }

            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }
}